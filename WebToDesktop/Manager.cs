

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.NetworkInformation;
using System.Threading.Tasks;
using Gtk;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using WebToDesktop.Forms;

namespace WebToDesktop
{
    public static class Manager
    {

        public static int WebPort;
        public static Application app;
        public static IHost Server;

        public static void RunServer<TStartUp>(string appid)
        where TStartUp : class
        {
            Application.Init();
            app = new Application(appid, GLib.ApplicationFlags.None);
            app.Register(GLib.Cancellable.Current);
            
            Server = CreateHost<TStartUp>();
            Server.StartAsync();
            Console.WriteLine("Host Running");
            
            // WebView2 w = new WebView2();
            // w.ShowAll();
            
            Application.Run();
            ExitWhenFinished();
        }

        private static IHost CreateHost<T>() where T : class
        {
            WebPort = FindPortNumber();
            HostBuilder builder = new HostBuilder();
            builder.ConfigureWebHostDefaults(con =>
                con.UseStartup<T>()
                    .UseWebRoot($"{Directory.GetCurrentDirectory()}/wwwroot/")
                    .UseUrls($"http://localhost:{WebPort}/")

            );
            Console.WriteLine($"Working Dir: {Directory.GetCurrentDirectory()}/wwwroot/");
            Console.WriteLine($"main url: http://localhost:{WebPort}/");
            return builder.Build();
        }

        private static void ExitWhenFinished()
        {
            Console.WriteLine("Closing Server");
            Server.Dispose();
            Server.StopAsync().Wait();
            Console.WriteLine("Bye");
        }

        private static int FindPortNumber()
        {
            int PortStartIndex = 2000;
            int PortEndIndex = 64000;
            IPGlobalProperties properties = IPGlobalProperties.GetIPGlobalProperties();
            IPEndPoint[] tcpEndPoints = properties.GetActiveTcpListeners();

            List<int> usedPorts = tcpEndPoints.Select(p => p.Port).ToList<int>();
            int unusedPort = 0;

            for (int port = PortStartIndex; port < PortEndIndex; port++)
            {
                if (!usedPorts.Contains(port))
                {
                    unusedPort = port;
                    break;
                }
            }

            return unusedPort;
        }

    }
}