using System;
using System.Threading.Tasks;
using Gdk;
using Gtk;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using TestApp.Data;
using WebToDesktop;
using WebToDesktop.Forms;
using Window = Gtk.Window;

namespace TestApp
{
    public class Startup
    {
        public static BrowserWindow MainWindow;
        public Startup(IConfiguration configuration)
        {
            Console.WriteLine("Hello Wolrd ctor");
            Configuration = configuration;
            Console.WriteLine("Hello Wolrd ctor");
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddRazorPages();
            services.AddServerSideBlazor();
            services.AddSingleton<WeatherForecastService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            Console.WriteLine("hello");
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            
            app.UseStaticFiles();

            app.UseRouting();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapBlazorHub();
                endpoints.MapFallbackToPage("/_Host");
            });
            Console.WriteLine("Hello Wolrd General 1");
            var w2 = new Window("hello");
            
            BrowserWindow w = new BrowserWindow("/", 1000, 800);
            Manager.app.AddWindow(w2);
            Console.WriteLine("Hello Wolrd General 2 ");

            w.ShowAsync();
            Console.WriteLine("Hello Wolrd General 0");
        }
    }
}